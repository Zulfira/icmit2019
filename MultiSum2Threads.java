package labs.threads;

import java.util.Random;

public class MultiSum2Threads {

    private static final int A_SIZE = 10000;

    private static double sum = 0d;

    private static double[] a = new double[A_SIZE];



    public static void main(String[] args) {

        for (int i = 0; i < A_SIZE; i++ ) {
            a[i] = rnd();
        }

        long t_start = System.nanoTime();

        sum = 0d;
        for (int i = 0; i < A_SIZE; i++) {
            sum += a[i];
        }

        long t_end = System.nanoTime() - t_start;
        System.out.println(t_end);
        System.out.println(sum);

        sum = 0d;

        Summator s1 = new Summator(0, A_SIZE/2, a);
        Summator s2 = new Summator(A_SIZE/2, A_SIZE, a);

        t_start = System.nanoTime();
        s1.start();
        s2.start();

        try {
            s1.join();
            s2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        sum += s1.getSum();
        sum += s2.getSum();

        t_end = System.nanoTime() - t_start;
        System.out.println(t_end);
        System.out.println(sum);

    }


    private static double rnd() {
        double leftLimit = 0D;
        double rightLimit = 1000D;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

}

