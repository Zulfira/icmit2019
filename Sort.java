package obrv;
interface Comparable {
	public int compare(Comparable c);
		
}

class CImpl implements Comparable{
	int value;
	
	public CImpl(int value) {
		this.value = value;
	}
	
	@Override
	public int compare(Comparable c) {
		if (this.value < ((CImpl)c).value) return -1;
		else if (this.value > ((CImpl)c).value) return 1;
		else return 0;
	}
}

class AImpl implements Comparable{
	float value;
	
	public AImpl(float value) {
		this.value = value;
	}
	
	@Override
	public int compare(Comparable c) {
		
		float e = 0.000000001f;
		
		if (Math.abs(this.value - ((AImpl)c).value) < e) return 0;
		else if (this.value < ((AImpl)c).value) return -1;
		else return 1;
	}
}


public class Sort {

	public static void main(String[] args) {
		Sort sort = new Sort();
		CImpl[] c = {new CImpl(-1), new CImpl(-2), new CImpl(4), new CImpl(7), new CImpl(2)};
		sort.qsort(c);
		for (int i = 0; i < c.length; i++)
		System.out.println(c[i].value);

		AImpl[] a = {new AImpl(9.2f), new AImpl(2.5f), new AImpl(4.23f)};
		sort.qsort(a);
		for (int i = 0; i < a.length; i++)
		System.out.println(a[i].value);
		
		Comparable y = new CImpl(0);
		System.out.println(sort.search(c, y));

		y = new AImpl(4.229999999999f);
		System.out.println(sort.search(a, y));

	}

	void qsort(Comparable[] a) {		
		for (int i = 1; i < a.length; i++) {
			int j = i - 1;
				while (j >= 0 && a[j].compare(a[j + 1])==1) {
					Comparable temp = a[j];
					a[j] = a[j + 1];
					a[j + 1] = temp;
					j--;
							}
				
		}
	}
	
	public int search(Comparable[] b, Comparable y) {
		int l=-1;
		int r=b.length;
		while (r>l+1) {
			int m=(l+r)/2;
			if (y.compare(b[m])==1)// y > b[m]
				l=m;
			else r=m;
		}
		if(r < b.length && b[r].compare(y)==0) // b[r] == y
		return r;
		else
		return -1;
	}
}
