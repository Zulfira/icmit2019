public class Sinc1 {

    public static void main(String[] args) {
        C2 t1 = new C2();
        C2 t2 = new C2();
        t1.start();
        t2.start();
    }
}

class C2 extends Thread {

    private static C3 v= new C3();

    public void run() {
        System.out.println("called m1 from " + Thread.currentThread().getName()+", v="+v.inc());
    }
}

class C3 {
    private int i=0;
    public int inc() {
        synchronized (this) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return ++i;
        }
    }
}