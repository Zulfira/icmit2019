public class StopThread1 {
    public static void main(String[] s) throws InterruptedException {

        T1 t = new T1();
        t.start();
        Thread.sleep(5000);
        t.setIsStoped();

    }
}

class T1 extends Thread {
    private volatile static boolean isStoped = false;

    public void setIsStoped() {
        isStoped = true;
    }

    @Override
    public void run() {
        while (!isStoped) {
            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("is Stoped = " + isStoped);
        }
    }
}