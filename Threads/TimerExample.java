import java.util.Timer;
import java.util.TimerTask;

public class TimerExample {

    public static void main(String[] args) {
        Timer timer = new Timer();
        Task1 task1 = new Task1();

        timer.schedule(task1, 0, 1000);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        timer.cancel();
    }
}

class Task1 extends TimerTask {
    public void run() {
        System.out.println("Task1 working");
    }
}
