public class StopThread2{
    public static void main(String[] s) throws InterruptedException {
        T2 t = new T2();
        t.start();
        Thread.sleep(5000);
        System.out.println("Send Interrupt signal");
        t.interrupt();
    }
}

class T2 extends Thread {
    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
                break; // or Thread.currentThread().interrupt();
            }
            System.out.println("This thread  works");
        }
        System.out.println("This thread is stopped");
    }
}