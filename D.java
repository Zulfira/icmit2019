package obrv;

public class D {

	public static void main(String[] args) {
		C h = new C(new B() {
			public int ex(int a, int b) {
				int c = a - b;

				System.out.println(c);
				return c;

			}
		});
		C j = new C(new B() {
			public int ex(int a, int b) {
				int c = a * b;

				System.out.println(c);
				return c;

			}
		});
		h.cl();
        j.cl();
	}
}
