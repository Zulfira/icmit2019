package labs.threads;

import java.util.Random;

public class MultiSum {

    private static final int A_SIZE = 10000;

    public static void main(String[] args) {

        double[] a = new double[A_SIZE];
        double sum = 0;

        for (int i = 0; i < A_SIZE; i++ ) {
            a[i] = rnd();
        }

        long t_start = System.nanoTime();
        for (int i = 0; i < A_SIZE; i++) {
            sum += a[i];
        }
        System.out.println(System.nanoTime() - t_start);
        System.out.println(sum);
    }

    private static double rnd() {
        double leftLimit = 0D;
        double rightLimit = 1000D;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }
}
