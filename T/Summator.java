package labs.threads;

class Summator extends Thread {
    private int start;
    private int end;
    private double local_sum;
    double[] a;

    public Summator(int start, int end, double[] a) {
        this.start = start;
        this.end = end;
        this.a = a;
    }

    @Override
    public void run() {
        local_sum = 0;
        for (int i = start; i < end; i++) {
            local_sum += a[i];
        }

        System.out.println(this.getName() +" end");
    }

    public double getSum() {
        return local_sum;
    }
}
