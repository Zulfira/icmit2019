package class1;

public class Class2 {

	public double cy(double r0, double r1, double[] w) {
		double y = 0;

		y += w[0] * r0 + w[1] * r0;
		y = f(y);

		return y;
	}

	public double f(double s) {
		double t = 1 / (1 + Math.exp(-s));
		return t;
	}
}